import {Button} from '../../components/button'
import winGold from '../../image/winGold.png'
import './style.scss'

function WinScreen ({valueGold}){
 

    return(
        <div className="winContainer">   
            <img src={winGold} alr="win"/>
            <h1 className="winTittle"> {valueGold} coins</h1>
            <Button className="winButton" id='test'>
              GET COINS
            </Button>
        </div>
        
    )
}

export {
    WinScreen
}
