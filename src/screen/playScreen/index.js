import {useState, useEffect} from 'react'
import {CardWrapper} from '../../components/cardWrapper'
import {Button} from '../../components/button'
import {Overlay} from '../../components/overlay'
import logo from '../../image/logo.png'
import './style.scss'

function PlayScreen ({arrEl, value, handleChangeValue}) {
    const [isOpenCard, setOpenCard] = useState(false);
    const [showButton, setShowButton] = useState(true)
    const [isOverlay, setIsOverlay] = useState(false);
    const [moveCard, setMoveCard] = useState(false);
    const [activeCard, setActiveCard] = useState(null)

     async function handleChooseCard(event, id) {
        if (moveCard){
          await setActiveCard(id)
        
            let meta;
            setTimeout(()=>{
                meta = event.target.parentNode.parentNode.getAttribute("data-value");
                handleChangeValue(meta);
                ;
            },1500)
            console.log(`VALUE ${value}`)
            console.log(`ID ${activeCard}, id ${id}`)
            return meta, activeCard  
        } 
        
      }

function playCard(){
    setOpenCard(true)
    setShowButton(false)

    setTimeout(()=>{
        setOpenCard(false)
    }, 2000)

    setTimeout(()=>{
        setIsOverlay(true)
    }, 3000)
     setTimeout(()=>{
        setIsOverlay(false)
     }, 5000)
    if(!isOpenCard){
        setMoveCard(true)
    }

}
    return(
        <div className="playContainer">
            <div className='logo'>
                <img src={logo} alt='Gold Digger'/>
            </div>
            <CardWrapper 
                arrGold={arrEl}
                openClass={isOpenCard? "flip": ""}
                moveClass={moveCard? "move": ""}
                move={moveCard? true: false} 
                chooseCard={handleChooseCard}
                active={activeCard}
            />
            <Button className={showButton? "": "hide"} onClick={playCard} >
                show
            </Button>
            <Overlay className={isOverlay? "show_overlay": "hide_overlay" }/>

        </div>
         
    )
}

export{
    PlayScreen
}