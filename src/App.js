import {useState, useEffect} from 'react'
// import {CardWrapper} from './components/cardWrapper'
// import {Button} from './components/button'
import {PlayScreen} from './screen/playScreen'
import {WinScreen} from './screen/winScreen'
import gold1 from './image/gold1.png'
import gold2 from './image/gold2.png'
import gold3 from './image/gold3.png'
import './App.scss';
import man from './image/man.png'


let goldsArr=[
  {img: gold1, data: 200, id: 0, className: "card"},
  {img: gold2, data: 500, id: 1, className: "card"}, 
  {img: gold3, data: 1000, id: 2, className: "card"}
 ];

function App() {
  const [data, setData] = useState(null);
  
  function handleGold(gold){
    setData(gold)
    console.log('gold',gold)
   }
 
   useEffect(()=>console.log('data',data),[])
  

  return (
    <div className="App">
    <div style={{opacity:data!==null?1:0,position:'absolute',zIndex:data!==null?2:-1}}><WinScreen valueGold={data}/></div>
    <div style={{opacity:data!==null?0:1,position:'absolute',zIndex:data!==null?-1:2}}><PlayScreen
        arrEl={goldsArr}
        value={data}
        handleChangeValue={handleGold}
       /></div>
      {/* <WinScreen valueGold={data}/> */}
      {/* {data? <WinScreen valueGold={data}/>: 
       
      } */}
      <img src={man} alt="man"  className="man"/>
</div>
    
  );
}

export default App;
