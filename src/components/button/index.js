import './style.scss';

function Button({ children, ...props }) {
  const classNames = props.className ? `button ${props.className}` : 'button';
  
  return (
    <button
      {...props}
      className={classNames}
      id={props.id}

    >
      {children}
    </button>
  )
}

export {
  Button
}