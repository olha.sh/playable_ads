import {Card} from '../card'
import './style.scss'
import { useEffect, useState } from 'react'



function CardWrapper({arrGold, openClass, moveClass, move, chooseCard, active}) {

  function shuffle(arr){
    let j, temp;
    for(let i = arr.length - 1; i > 0; i--){
      j = Math.floor(Math.random()*(i + 1));
      temp = arr[j];
      arr[j] = arr[i];
      arr[i] = temp;
    }
    return arr;
  }
  let arr;
move? arr = arrGold: arr = shuffle(arrGold)
const classNames = openClass +` ${moveClass}`;
    return(
            <ul className='cardWrapper' >
              {arr.map((item) =>{
             return(
                 <Card 
                itemID={item.id}
                className={classNames} 
                frontImg={item.img} 
                data={item.data}
                choose={chooseCard}
                activeClass={active}/>
             ) 
            })}
          </ul>
    )
}

export{
    CardWrapper
}