import './style.scss'

function Overlay({className}) {
    const classNames= className? `overlay ${className}`: "overlay";
    return(
        <div className={classNames}>
            <h1>pick your bonus</h1>
        </div>
    )
}
export{
    Overlay
}