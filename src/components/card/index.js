import './style.scss'
import cardStart from '../../image/cardStart.png'


function Card ({itemID, frontImg, data, className,choose, activeClass}) {

    return(
        <div className="card" id={itemID} data-value={data} onClick={(event)=>choose(event, itemID)}>
            <div className={"front " + className +  (itemID === activeClass ? " active_item" : "")}>
                <img src={frontImg} alt='card' />
            </div>
            <div className={"back " + className +  (itemID === activeClass ? " active_item" : "")} >
                <img src={cardStart} alt='card'/>
            </div>
        </div>  
    )
}

export{
    Card
}